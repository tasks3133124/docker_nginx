#!/bin/bash

SSL_CER_FILE="/etc/nginx/ssl/localhost.crt"
# Интервал времени для проверки сертификата (проверяется на настоящий момент)
EXPIRE_DAYS=$(( 24*3600*0 ))
# Через 90 дней сертификат будет просрочен
#EXPIRE_DAYS=$(( 24*3600*90 ))

CHECK_CERT=$(cat $SSL_CER_FILE | openssl x509 -checkend $EXPIRE_DAYS)
IS_CERT_EXPIRED=$(echo $?)

if [ "$IS_CERT_EXPIRED" -ne "0" ]
then
    echo -e "Certificate is expired!"
    exit 1
fi
