#!/bin/bash

# Generate self-signed certificates for localhost (сгенерировать самоподписанный ssl сертификат на один домен для localhost).
# Нужен для myserver.conf
# https://letsencrypt.org/docs/certificates-for-localhost/
# Сертификат будет действителен в течении 30 дней с момента генерации

# Абсолютный (полный) путь текущего скрипта
ABSOLUTE_SCRIPT_PATH=$(readlink -e "$0")
# Абсолютный (полный) путь каталога текущего скрипта
ABSOLUTE_SCRIPT_DIR_PATH=$(dirname "$ABSOLUTE_SCRIPT_PATH")
# Абсолютный (полный) путь каталога  docker-проекта и каталога scripts в нем
if [ -f "$ABSOLUTE_SCRIPT_DIR_PATH/docker-compose.yml" ] && [ -d "$ABSOLUTE_SCRIPT_DIR_PATH/scripts" ]
then
    DOCKER_PROJECT_DIR="$ABSOLUTE_SCRIPT_DIR_PATH"
    DOCKER_PROJECT_SCRIPTS_DIR="$ABSOLUTE_SCRIPT_DIR_PATH/scripts"
elif [ -f "$ABSOLUTE_SCRIPT_DIR_PATH/../docker-compose.yml" ] && [ -d "$ABSOLUTE_SCRIPT_DIR_PATH/../scripts" ]
then
    DOCKER_PROJECT_DIR=$(dirname "$ABSOLUTE_SCRIPT_DIR_PATH")
    DOCKER_PROJECT_SCRIPTS_DIR=$(dirname "$ABSOLUTE_SCRIPT_DIR_PATH")"/scripts"
else
    exit 1
fi

LOCALHOST_DIR="$DOCKER_PROJECT_DIR/ssl"

mkdir -p $LOCALHOST_DIR && \
    openssl req -x509 -out $LOCALHOST_DIR/localhost.crt -keyout $LOCALHOST_DIR/localhost.key \
            -newkey rsa:2048 -nodes -sha256 \
            -subj '/CN=localhost' -extensions EXT -config <( \
             printf "[dn]\nCN=localhost\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:localhost\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")
